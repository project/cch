Introduction
=================
The Custom Contextual Help module provides the ability
to create help content from a contectual block and display
the related help content by, node, content type and view.

Installation
==================
- Download the CCH module and enable.
- Define permissions for who can administer cch.
- Navigate to the blocks page and verify the new Custom 
  Content Help block is in the help region, or place it in
  region of your choice.
- Navigate to a page or content type with a user that has
  administrater permissions and toggle open the help block
  to add Custom Contextual Help for the item or Content Type.

TODO
==================
- Need to imrprove page detection in CCHContext class!